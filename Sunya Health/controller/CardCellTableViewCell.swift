//
//  CardCellTableViewCell.swift
//  Sunya Health
//
//  Created by macOs Catalina on 1/30/21.
//

import UIKit

class CardCellTableViewCell: UITableViewCell {

    
    @IBOutlet weak var cardView: UIView!
    
    @IBOutlet weak var planName: UILabel!
    @IBOutlet weak var planNameValue: UILabel!
    @IBOutlet weak var vitalTests: UILabel!
    @IBOutlet weak var vitalTestValue: UILabel!
    @IBOutlet weak var renewablePeriod: UILabel!
    @IBOutlet weak var renewablePeriodValue: UILabel!
    @IBOutlet weak var cost: UILabel!
    @IBOutlet weak var costValue: UILabel!
    
    //
    func configure(nameOfPlan: String,numOfProvidedTest: String,renewable: String,costOfPlan: String){
        planName.text = "Plan"
        planNameValue.text = nameOfPlan
        vitalTests.text = "No.of Vital Tests"
        vitalTestValue.text = numOfProvidedTest
        renewablePeriod.text = "Renewable"
        renewablePeriodValue.text = renewable
        cost.text = "Cost"
        costValue.text = "NRS " + costOfPlan
        
        cardView.layer.shadowColor = UIColor.gray.cgColor
        cardView.layer.shadowOffset = CGSize(width: 1.0,height: 1.0)
        cardView.layer.shadowOpacity = 1.0
        cardView.layer.masksToBounds = false
        cardView.layer.cornerRadius = 2.0
        
    }
}
