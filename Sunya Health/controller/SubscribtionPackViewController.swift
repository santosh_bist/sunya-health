//
//  SubscribtionPackViewController.swift
//  Sunya Health
//
//  Created by macOs Catalina on 1/30/21.
//

import UIKit

class SubscribtionPackViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var cardTableView: UITableView!
//    let columnTitle: [String] = ["Plan","NO. of Vital Tests","Renewable","Cost"]
    let nameOfPlan: [String] = ["Basic","Advance","Premium"]
    let numOfProvidedTest: [String] = ["7","10","20"]
    let renewable: [String] = ["Per Month","Per Month","Per Month"]
    let costOfPlan: [String] = ["500.00","700.00","1000.00"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    //This acdtion is for back arrow button
        @IBAction func backToLogin(_ sender: Any) {
            guard let backToLoginScreen = storyboard?.instantiateViewController(withIdentifier: "LoginStoryboard")  else { return}
            backToLoginScreen.modalPresentationStyle = .fullScreen
            backToLoginScreen.modalTransitionStyle = .flipHorizontal
            self.present(backToLoginScreen,animated: true)
    }
    //This action is for logout event
        @IBAction func afterLogout(_ sender: Any) {
        
            guard let toLoginScreen = storyboard?.instantiateViewController(withIdentifier: "LoginStoryboard")  else { return}
            toLoginScreen.modalPresentationStyle = .fullScreen
            toLoginScreen.modalTransitionStyle = .flipHorizontal
            self.present(toLoginScreen,animated: true)
    }
    //How many rows
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameOfPlan.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cardCell", for: indexPath) as! CardCellTableViewCell
        cell.configure(nameOfPlan: nameOfPlan[indexPath.row], numOfProvidedTest: numOfProvidedTest[indexPath.row], renewable: renewable[indexPath.row], costOfPlan: costOfPlan[indexPath.row])
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dataToCheckOut = storyboard?.instantiateViewController(withIdentifier: "checkoutStoryboard") as? CheckoutViewController
        dataToCheckOut?.planCost = costOfPlan[indexPath.row]
        dataToCheckOut?.modalPresentationStyle = .fullScreen
        self.present(dataToCheckOut!, animated: true)
    }
}
