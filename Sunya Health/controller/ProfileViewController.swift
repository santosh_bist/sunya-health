//
//  ProfileViewController.swift
//  Sunya Health
//
//  Created by macOs Catalina on 1/29/21.
//

import UIKit

class ProfileViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func profileToMenuTapped(_ sender: Any) {
        guard let profileToMenu = storyboard?.instantiateViewController(withIdentifier: "menuStoryboard")  else { return}        
        profileToMenu.modalPresentationStyle = .fullScreen
        profileToMenu.modalTransitionStyle = .flipHorizontal
        self.present(profileToMenu,animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
