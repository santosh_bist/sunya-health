//
//  RegisterViewController.swift
//  Sunya Health
//
//  Created by macOs Catalina on 1/23/21.
//

import UIKit

class RegisterViewController: UIViewController {

   
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var emailRegisterField: UITextField!
    @IBOutlet weak var passwordRegisterField: UITextField!
    @IBOutlet weak var confirmPasswordRegisterField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //for first name
        firstNameField.layer.cornerRadius = 3
        firstNameField.layer.shadowColor = UIColor(red: 0/250.0,green: 0/250.0,blue: 0/250.0,alpha: 1.0).cgColor
        firstNameField.layer.shadowOffset = CGSize(width: 0, height: 1.75)
        firstNameField.layer.shadowRadius = 1.7
        firstNameField.layer.shadowOpacity = 0.45
        
        //for last name
        lastNameField.layer.cornerRadius = 3
        lastNameField.layer.shadowColor = UIColor(red: 0/250.0,green: 0/250.0,blue: 0/250.0,alpha: 1.0).cgColor
        lastNameField.layer.shadowOffset = CGSize(width: 0, height: 1.75)
        lastNameField.layer.shadowRadius = 1.7
        lastNameField.layer.shadowOpacity = 0.45
        
        //for phone
        phoneNumberField.layer.cornerRadius = 3
        phoneNumberField.layer.shadowColor = UIColor(red: 0/250.0,green: 0/250.0,blue: 0/250.0,alpha: 1.0).cgColor
        phoneNumberField.layer.shadowOffset = CGSize(width: 0, height: 1.75)
        phoneNumberField.layer.shadowRadius = 1.7
        phoneNumberField.layer.shadowOpacity = 0.45
        
        //for email
        emailRegisterField.layer.cornerRadius = 3
        emailRegisterField.layer.shadowColor = UIColor(red: 0/250.0,green: 0/250.0,blue: 0/250.0,alpha: 1.0).cgColor
        emailRegisterField.layer.shadowOffset = CGSize(width: 0, height: 1.75)
        emailRegisterField.layer.shadowRadius = 1.7
        emailRegisterField.layer.shadowOpacity = 0.45
        
        //for password
        passwordRegisterField.layer.cornerRadius = 3
        passwordRegisterField.layer.shadowColor = UIColor(red: 0/250.0,green: 0/250.0,blue: 0/250.0,alpha: 1.0).cgColor
        passwordRegisterField.layer.shadowOffset = CGSize(width: 0, height: 1.75)
        passwordRegisterField.layer.shadowRadius = 1.7
        passwordRegisterField.layer.shadowOpacity = 0.45
        
        //for confirm-password
        confirmPasswordRegisterField.layer.cornerRadius = 3
        confirmPasswordRegisterField.layer.shadowColor = UIColor(red: 0/250.0,green: 0/250.0,blue: 0/250.0,alpha: 1.0).cgColor
        confirmPasswordRegisterField.layer.shadowOffset = CGSize(width: 0, height: 1.75)
        confirmPasswordRegisterField.layer.shadowRadius = 1.7
        confirmPasswordRegisterField.layer.shadowOpacity = 0.45
        
        //for register button
        registerButton.layer.cornerRadius = 3
        registerButton.layer.shadowColor = UIColor(red: 0/250.0,green: 0/250.0,blue: 0/250.0,alpha: 1.0).cgColor
        registerButton.layer.shadowOffset = CGSize(width: 0, height: 1.75)
        registerButton.layer.shadowRadius = 1.7
        registerButton.layer.shadowOpacity = 0.45
    }
    
    @IBAction func toLoginView(_ sender: Any) {
        guard let loginViewController = storyboard?.instantiateViewController(withIdentifier: "LoginStoryboard")  else { return}
        loginViewController.modalPresentationStyle = .fullScreen
        loginViewController.modalTransitionStyle = .flipHorizontal
        self.present(loginViewController,animated: true)

    }
    
    @IBAction func toHomeScreen(_ sender: Any) {
    
    guard let homeViewController = storyboard?.instantiateViewController(withIdentifier: "subscribtionStoryboard")  else { return}
    homeViewController.modalPresentationStyle = .fullScreen
    homeViewController.modalTransitionStyle = .flipHorizontal
    self.present(homeViewController,animated: true)
    }
}
