//
//  CheckoutViewController.swift
//  Sunya Health
//
//  Created by macOs Catalina on 1/24/21.
//

import UIKit
import Khalti
class CheckoutViewController: UIViewController, KhaltiPayDelegate {
    func onCheckOutSuccess(data: Dictionary<String, Any>) {
        print("Payment Successful!")
    }
    
    func onCheckOutError(action: String, message: String, data: Dictionary<String, Any>?) {
        print("Sorry!!!!")
    }
    

    @IBOutlet weak var costOfPlan: UILabel!
    var planCost = ""
    var amountAfter = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        costOfPlan.text = planCost
        amountAfter = planCost
        
    }
    // This acion leads us back to scubscribtion packages
        @IBAction func backToSubcribtionScreen(_ sender: Any) {
            guard let backToSubscribtion = storyboard?.instantiateViewController(withIdentifier: "subscribtionStoryboard")  else { return}
            backToSubscribtion.modalPresentationStyle = .fullScreen
            self.present(backToSubscribtion,animated: true)
        }
    /// on confirmation of payment KHALTI payment method triggered
        @IBAction func toConfirm(_ sender: Any) {
            let num = (amountAfter as NSString).doubleValue
            let num1 = Int(num)
            
            
            let TEST_CONFIG:Config = Config(publicKey: "test_public_key_9b3acc565a8b4b2ab141b139fb9fc688", amount: num1*100, productId: "test", productName: "Dragon_boss")
            // Data passed here are based on Example project
            Khalti.present(caller: self, with: TEST_CONFIG, delegate: self)
            
        }
    
}

