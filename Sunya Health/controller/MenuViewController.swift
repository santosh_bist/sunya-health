//
//  MenuViewController.swift
//  Sunya Health
//
//  Created by macOs Catalina on 1/28/21.
//

import UIKit
enum MenuType: Int {
    case home
    case profile
    case report
    case subscribtion
    case logout
    
}
class MenuViewController: UITableViewController {
    var didTapMenuType: ((MenuType) -> Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       guard let menuType = MenuType(rawValue: indexPath.row) else {return}
        dismiss(animated: true){ [weak self] in
            print("Dismissing: \(menuType)")
            self?.didTapMenuType?(menuType)
        }
    }

}
