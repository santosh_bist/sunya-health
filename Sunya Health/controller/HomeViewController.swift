//
//  HomeViewController.swift
//  Sunya Health
//
//  Created by macOs Catalina on 1/28/21.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet var tableView: UITableView!
    
    let transition = SlideInTransition();
    var topView: UIView?;
    //MarK: variable initialization for shadow effects
    
    @IBOutlet weak var testButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //for Take a test button shadow
            testButton.layer.cornerRadius = 3
            testButton.layer.shadowColor = UIColor(red: 0/250.0,green: 0/250.0,blue: 0/250.0,alpha: 1.0).cgColor
            testButton.layer.shadowOffset = CGSize(width: 2, height: 5.75)
            testButton.layer.shadowRadius = 1.7
            testButton.layer.shadowOpacity = 0.1
       
    }
    @IBAction func didTappedMenu(_ sender: Any) {
    
        guard let menuViewController = storyboard?.instantiateViewController(withIdentifier: "menuStoryboard") as? MenuViewController else { return}
        
        menuViewController.didTapMenuType = { menuType in
            self.transitionToNew(menuType);
        }
        menuViewController.modalPresentationStyle = .overCurrentContext;
        menuViewController.transitioningDelegate = self
        
        present(menuViewController, animated: true)
    }
    
    
    func transitionToNew(_ menuType: MenuType){
        
        let title = String(describing: menuType).capitalized
        self.title = title
        
        topView?.removeFromSuperview()
        switch menuType{
        case .profile:
            let view = UIView()
//            view.backgroundColor = .yellow
            view.frame = self.view.bounds
            self.view.addSubview(view)
            self.topView = view

            guard let profileViewController = storyboard?.instantiateViewController(withIdentifier: "profileStoryboard") else { return}
            addChild(profileViewController)
            view.addSubview(profileViewController.view)
            profileViewController.didMove(toParent: self)
            
        case .report:
            let view = UIView()
//            view.backgroundColor = .green
            view.frame = self.view.bounds
            self.view.addSubview(view)
            self.topView = view

            guard let reportViewController = storyboard?.instantiateViewController(withIdentifier: "reportStoryboard") else { return}
            addChild(reportViewController)
                view.addSubview(reportViewController.view)
            reportViewController.didMove(toParent: self)
            
        case .subscribtion:
            let view = UIView()
//            view.backgroundColor = .blue
            view.frame = self.view.bounds
            self.view.addSubview(view)
            self.topView = view
            
            guard let subscribtionViewController = storyboard?.instantiateViewController(withIdentifier: "subscribedStoryboard") else { return}
            addChild(subscribtionViewController)
            view.addSubview(subscribtionViewController.view)
            subscribtionViewController.didMove(toParent: self)
            
        case .logout:
//            let view = UIView()
//            view.backgroundColor = .red
//            view.frame = self.view.bounds
//            self.view.addSubview(view)
//            self.topView = view
            guard let onLogout = storyboard?.instantiateViewController(withIdentifier: "LoginStoryboard") else { return}
            addChild(onLogout)
            view.addSubview(onLogout.view)
            onLogout.didMove(toParent: self)
            
        default:
            break
        }
    }
    

}
extension HomeViewController: UIViewControllerTransitioningDelegate{
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.isPresenting = true
        return transition
    }
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.isPresenting = false
        return transition
    }
}
