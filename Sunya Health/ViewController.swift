//
//  ViewController.swift
//  Sunya Health
//
//  Created by macOs Catalina on 1/12/21.
//

import UIKit

class ViewController: UIViewController {
    //declaration of email text field
        @IBOutlet weak var emailFieldLabel: UITextField!
    //declaration of password text field
        @IBOutlet weak var passwordFieldLabel: UITextField!
    //declaration of login button
        @IBOutlet weak var loginButton: UIButton!
    //for show/hide icon  button
        let button = UIButton(type: .custom)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //for email field shadow
            emailFieldLabel.layer.cornerRadius = 3
            emailFieldLabel.layer.shadowColor = UIColor(red: 0/250.0,green: 0/250.0,blue: 0/250.0,alpha: 1.0).cgColor
            emailFieldLabel.layer.shadowOffset = CGSize(width: 0, height: 1.75)
            emailFieldLabel.layer.shadowRadius = 1.7
            emailFieldLabel.layer.shadowOpacity = 0.45
        
        //for password field shadow
            passwordFieldLabel.layer.cornerRadius = 3
            passwordFieldLabel.layer.shadowColor = UIColor(red:0/250.0,green: 0/250.0,blue: 0/250.0,alpha: 1.0).cgColor
            passwordFieldLabel.layer.shadowOffset = CGSize(width: 0, height: 1.75)
            passwordFieldLabel.layer.shadowRadius = 1.7
            passwordFieldLabel.layer.shadowOpacity = 0.45
        //for login Button shadow
            loginButton.layer.cornerRadius = 3
            loginButton.layer.shadowColor = UIColor(red:0/250.0,green: 0/250.0,blue: 0/250.0,alpha: 1.0).cgColor
            loginButton.layer.shadowOffset = CGSize(width: 0, height: 1.75)
            loginButton.layer.shadowRadius = 1.7
            loginButton.layer.shadowOpacity = 0.45
        
        //for hide and show of password text
            passwordFieldLabel.rightViewMode = .unlessEditing
            
            button.setImage(UIImage(named: "eye-closed.png"), for: .normal)
            button.imageEdgeInsets = UIEdgeInsets(top:5, left: -24,bottom: 5, right: 15)
            button.frame = CGRect(x: CGFloat(passwordFieldLabel.frame.size.width - 25), y: CGFloat(5), width: CGFloat(15), height: CGFloat(25))
            button.addTarget(self, action: #selector(self.btnPasswordVisibilityClicked), for: .touchUpInside)
            passwordFieldLabel.rightView = button
            passwordFieldLabel.rightViewMode = .always
    }
    
    // ACTION FOR LOGIN
    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    var givenEmail = "sa@gmail"
    var givenPassword = "pa"
    
    
    @IBAction func toLogin(_ sender: Any) {
        let emailFieldText = emailText.text
        let passwordFieldText = passwordText.text
        if (emailFieldText == "" || passwordFieldText == ""){
            return
        }
        doLogin(emailFieldText!, passwordFieldText!)
    }
    func doLogin(_ email: String,_ psw: String){
        if(email == givenEmail && psw == givenPassword){
            performSegue(withIdentifier: "home_segue", sender: nil)
            guard let homeViewController = storyboard?.instantiateViewController(withIdentifier: "homeStoryboard")  else { return}
            homeViewController.modalPresentationStyle = .fullScreen
//            self.present(subscribtionViewController,animated: true)
        }else {
//            performSegue(withIdentifier: "home_segue", sender: nil)
            guard let subscribtionViewController = storyboard?.instantiateViewController(withIdentifier: "subscribtionStoryboard")  else { return}
            subscribtionViewController.modalPresentationStyle = .fullScreen
            self.present(subscribtionViewController,animated: true)
        }
    }
    
    // Action for REGISTRATION (NEW-USERS)
        @IBAction func accountRegister(_ sender: Any) {
        
            guard let registerViewController = storyboard?.instantiateViewController(withIdentifier: "RegisterStoryboard")  else { return}
            registerViewController.modalPresentationStyle = .fullScreen
            self.present(registerViewController,animated: true)
        }
    
    //for function btnPasswordVisibilityClicked
        @IBAction func btnPasswordVisibilityClicked(_ sender: Any){
            (sender as! UIButton).isSelected = !(sender as! UIButton).isSelected
            if (sender as! UIButton).isSelected{
                self.passwordFieldLabel.isSecureTextEntry = false
                button.setImage(UIImage(named: "eye_open.png"), for: .normal)
            }else {
                self.passwordFieldLabel.isSecureTextEntry = true
                button.setImage(UIImage(named: "eye-closed.png"),for: .normal)
            }
    }


}

